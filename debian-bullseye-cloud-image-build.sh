#!/bin/bash
set -Eeuo pipefail

# Build a new Debian bullseye image which works around the problem that
# when cloud-init is fed a static IPv6 config, ifup/ifdown will fail with:
# "/etc/network/if-pre-up.d/cloud_inet6: 12: IF_TRY_DHCP: parameter not set"
#
# Requirements:
# - wget
# - qemu-utils
# - psmisc
#
# Notes:
# - Run with root privilege
# - Caveats exist
# - Will break your host OS, run on a temporary machine
#
# References:
# - https://forums.debian.net/viewtopic.php?f=5&t=149963
# - https://www.mail-archive.com/debian-cloud@lists.debian.org/msg05472.html
# - https://bugs.launchpad.net/cloud-init/+bug/1863773

IMG_SRC="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2"
export DEBIAN_FRONTEND=noninteractive

>&2 echo "[*] Download image"
wget --continue -O disk0.qcow2 "$IMG_SRC"

>&2 echo "[*] Mount image"
modprobe nbd
qemu-nbd -c /dev/nbd0 disk0.qcow2
sleep 5
partprobe /dev/nbd0 || true
mount /dev/nbd0p1 /mnt

>&2 echo "[*] Inject config"
cat > /mnt/etc/apt/sources.list.d/testing.list <<EOF
deb http://deb.debian.org/debian testing main non-free contrib
deb-src http://deb.debian.org/debian testing main non-free contrib
deb http://deb.debian.org/debian unstable main non-free contrib
deb-src http://deb.debian.org/debian unstable main non-free contrib
EOF

cat > /mnt/etc/apt/preferences.d/testing <<EOF
Package: *
Pin: release a=stable
Pin-Priority: 500

Package: *
Pin: release a=testing
Pin-Priority: 100

Package: *
Pin: release a=unstable
Pin-Priority: 50
EOF

mkdir -p /mnt/etc/systemd/system/networking.service.d
cat > /mnt/etc/systemd/system/networking.service.d/fuck-cloud-init.conf <<EOF
[Service]
ExecStartPre=-/bin/sh -c "/bin/rm /run/network/interfaces.d/*"
EOF

rm /mnt/etc/resolv.conf
cat /etc/resolv.conf > /mnt/etc/resolv.conf
chroot /mnt apt-get update -y
chroot /mnt apt-get install -y qemu-guest-agent acpid
chroot /mnt apt-get install -y -t testing cloud-init

>&2 echo "[*] Cleanup"
chroot /mnt apt-get clean -y
rm -rf /mnt/etc/var/lib/apt/lists/* /mnt/etc/apt/preferences.d/testing /mnt/etc/apt/sources.list.d/testing.list /mnt/etc/network/if-{pre-up,post-down}.d/cloud_inet6
rm -rf /mnt/etc/resolv.conf
ln -s ../run/resolvconf/resolv.conf /mnt/etc/resolv.conf

>&2 echo "[*] Finalize image"
umount /mnt
sync
sleep 5
killall qemu-nbd

>&2 echo "[+] Done"
